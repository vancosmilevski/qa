﻿using QA.Model;

namespace Core
{
    public interface IQuestionVoteRepository : IRepository<QuestionVote>, IVoteRepository<QuestionVote>
    {
    //    QuestionVote GetByIds(int questionId, int userId);
    //    void Delete(int questionId, int userId);
    }
}
