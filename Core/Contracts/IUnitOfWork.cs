﻿using QA.Model;

namespace Core
{
    public interface IUnitOfWork
    {
        void Commit();

        IQuestionRepository Questions { get; }
        IRepository<Answer> Answers { get; }
        IRepository<Tag> Tags { get; }
        IRepository<QuestionsTag> QuestionsTags { get; }
        IQuestionVoteRepository QuestionVotes { get; }
        IAnswerVoteRepository AnswerVotes { get; }
        IUserRepository Users { get; }
    }
}
