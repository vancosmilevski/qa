﻿
namespace Core
{
    public interface IVoteRepository<T> where T : class
    {
        T GetByIds(int typeOfTId, int userId);
        void Delete(int typeOfTId, int userId);
    }
}
