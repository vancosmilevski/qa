﻿using QA.Model;

namespace Core
{
    public interface IAnswerVoteRepository : IRepository<AnswerVote>, IVoteRepository<AnswerVote>
    {
        //    QuestionVote GetByIds(int questionId, int userId);
        //    void Delete(int questionId, int userId);
    }
}
