﻿using QA.Model;
using System.Collections.Generic;

namespace Core
{
    public interface IQuestionRepository : IRepository<Question>
    {
        void AddQuestionTags(Question question, List<int> tagIDs);
    }
}