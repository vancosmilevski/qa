﻿using QA.Model;

namespace Core
{
    public interface IUserRepository : IRepository<User>
    {
        int CurrentUserId { get; }
        string CurrentUserUsername { get; }
    }
}
