﻿using QA.Model;
using System;
using System.Linq;

namespace Core
{
    public class AnswerVoteRepository : Repository<AnswerVote>, IAnswerVoteRepository
    {
        public AnswerVoteRepository(Entities context) : base(context) { }

        public AnswerVote GetByIds(int answerId, int userId)
        {
            return DbSet.FirstOrDefault(i => i.AnswerID == answerId && i.UserID == userId);
        }

        public override void Delete(int id)
        {
            throw new InvalidOperationException("Cannot delete a AnswerVote object by a single id value.");
        }

        public void Delete(int answerId, int userId)
        {
            var questionVote = GetByIds(answerId, userId);
            Delete(questionVote);
        }
    }
}
