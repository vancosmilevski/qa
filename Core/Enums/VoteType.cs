﻿
namespace Core
{
    public enum VoteType
    {
        Up = 1,
        Down = 2,
        Zero = 3
    }
}
