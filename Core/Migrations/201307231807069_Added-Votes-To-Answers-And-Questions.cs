namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedVotesToAnswersAndQuestions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AnswerVotes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Vote = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        AnswerID = c.Int(nullable: false),
                        DateVoted = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Answers", t => t.AnswerID)
                .ForeignKey("dbo.Users", t => t.UserID)
                .Index(t => t.AnswerID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.QuestionVotes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Vote = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                        QuestionID = c.Int(nullable: false),
                        DateVoted = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Questions", t => t.QuestionID)
                .ForeignKey("dbo.Users", t => t.UserID)
                .Index(t => t.QuestionID)
                .Index(t => t.UserID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.QuestionVotes", new[] { "UserID" });
            DropIndex("dbo.QuestionVotes", new[] { "QuestionID" });
            DropIndex("dbo.AnswerVotes", new[] { "UserID" });
            DropIndex("dbo.AnswerVotes", new[] { "AnswerID" });
            DropForeignKey("dbo.QuestionVotes", "UserID", "dbo.Users");
            DropForeignKey("dbo.QuestionVotes", "QuestionID", "dbo.Questions");
            DropForeignKey("dbo.AnswerVotes", "UserID", "dbo.Users");
            DropForeignKey("dbo.AnswerVotes", "AnswerID", "dbo.Answers");
            DropTable("dbo.QuestionVotes");
            DropTable("dbo.AnswerVotes");
        }
    }
}
