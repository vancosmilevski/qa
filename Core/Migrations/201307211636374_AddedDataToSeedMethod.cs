namespace Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDataToSeedMethod : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Answers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        VoteCount = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        DatePosted = c.DateTime(nullable: false),
                        UserID = c.Int(nullable: false),
                        QuestionID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Questions", t => t.QuestionID)
                .ForeignKey("dbo.Users", t => t.UserID)
                .Index(t => t.QuestionID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Questions",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        VoteCount = c.Int(nullable: false),
                        NumberOfViews = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Title = c.String(),
                        Content = c.String(),
                        DatePosted = c.DateTime(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Users", t => t.UserID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Username = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.QuestionsTags",
                c => new
                    {
                        QuestionID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                        DateAdded = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => new { t.QuestionID, t.TagID })
                .ForeignKey("dbo.Questions", t => t.QuestionID)
                .ForeignKey("dbo.Tags", t => t.TagID)
                .Index(t => t.QuestionID)
                .Index(t => t.TagID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.QuestionsTags", new[] { "TagID" });
            DropIndex("dbo.QuestionsTags", new[] { "QuestionID" });
            DropIndex("dbo.Questions", new[] { "UserID" });
            DropIndex("dbo.Answers", new[] { "UserID" });
            DropIndex("dbo.Answers", new[] { "QuestionID" });
            DropForeignKey("dbo.QuestionsTags", "TagID", "dbo.Tags");
            DropForeignKey("dbo.QuestionsTags", "QuestionID", "dbo.Questions");
            DropForeignKey("dbo.Questions", "UserID", "dbo.Users");
            DropForeignKey("dbo.Answers", "UserID", "dbo.Users");
            DropForeignKey("dbo.Answers", "QuestionID", "dbo.Questions");
            DropTable("dbo.Tags");
            DropTable("dbo.QuestionsTags");
            DropTable("dbo.Users");
            DropTable("dbo.Questions");
            DropTable("dbo.Answers");
        }
    }
}
