// <auto-generated />
namespace Core.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class AddedDataToSeedMethod : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedDataToSeedMethod));
        
        string IMigrationMetadata.Id
        {
            get { return "201307211636374_AddedDataToSeedMethod"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
