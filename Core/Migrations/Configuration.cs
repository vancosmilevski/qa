using System.Web.Security;

namespace Core.Migrations
{
    using QA.Model;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WebMatrix.WebData;

    internal sealed class Configuration : DbMigrationsConfiguration<Core.Entities>
    {
        private const string User1 = "vanco";
        private const string User2 = "frosina";
        private const string UserAdmin = "admin";

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Core.Entities context)
        {
            // order is important (WebSecurity.InitializeDatabaseConnection() has to be called before calling anything from WebSecurity namaspace such as SimpleMembershipProvider.GetUser DeleteUser ...)
            ClearData(context);

            InitMembership();

            ClearMembershipData();

            SeedMembershipData();

            SeedData(context);
        }

        private void ClearData(Entities context)
        {
            var questions = context.Questions.ToList();
            foreach (var q in questions)
            {
                context.Questions.Remove(q);
            }

            var answers = context.Answers.ToList();
            foreach (var a in answers)
            {
                context.Answers.Remove(a);
            }

            var tags = context.Tags.ToList();
            foreach (var t in tags)
            {
                context.Tags.Remove(t);
            }

            var qts = context.QuestionsTags.ToList();
            foreach (var qt in qts)
            {
                context.QuestionsTags.Remove(qt);
            }

            context.SaveChanges();
        }

        private void InitMembership()
        {
            WebSecConfig.RegisterWebSec();
        }

        private void ClearMembershipData()
        {
            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            roles.DeleteRole("Admin", false);

            membership.DeleteUser(User1, true);
            membership.DeleteUser(User2, true);
            membership.DeleteUser(UserAdmin, true);
        }

        private void SeedMembershipData()
        {
            var roles = (SimpleRoleProvider)Roles.Provider;
            var membership = (SimpleMembershipProvider)Membership.Provider;

            foreach (var user in new string[] { User1, User2, UserAdmin })
            {
                if (membership.GetUser(user, false) == null)
                {
                    membership.CreateUserAndAccount(user, user);
                }
            }

            if (!roles.RoleExists("Admin"))
            {
                roles.CreateRole("Admin");
            }

            if (!roles.GetRolesForUser(UserAdmin).Contains("Admin"))
            {
                roles.AddUsersToRoles(new[] { UserAdmin }, new[] { "Admin" });
            }
        }

        private void SeedData(Entities context)
        {
            List<int> users = context.Users.Where(i => i.Username == User1 || i.Username == User2).Select(i => i.ID).ToList();

            List<Tag> tags = AddTags(context);
            List<Question> questions = AddQuestions(context, users);

            AddQuestionTags(context, tags, questions);

            AddAnswers(context, questions, users);

        }

        private void AddAnswers(Entities context, List<Question> questions, List<int> usersIds)
        {
            var answer1 = new Answer() { Content = "First Answer", Status = 1, VoteCount = 0, DatePosted = DateTime.Now, UserID = usersIds.ElementAt(0) };
            var answer2 = new Answer() { Content = "Second Answer", Status = 1, VoteCount = 0, DatePosted = DateTime.Now, UserID = usersIds.ElementAt(0) };
            var answer3 = new Answer() { Content = "Third Answer", Status = 1, VoteCount = 0, DatePosted = DateTime.Now, UserID = usersIds.ElementAt(1) };
            var answer4 = new Answer() { Content = "Fourth Answer", Status = 1, VoteCount = 0, DatePosted = DateTime.Now, UserID = usersIds.ElementAt(1) };

            questions.First().Answers.Add(answer1);
            questions.First().Answers.Add(answer2);

            questions.ElementAt(1).Answers.Add(answer3);
            questions.ElementAt(1).Answers.Add(answer4);

            context.SaveChanges();
        }

        private void AddQuestionTags(Entities context, List<Tag> tags, List<Question> questions)
        {
            foreach (var q in questions)
            {
                foreach (var t in tags)
                {
                    context.QuestionsTags.Add(new QuestionsTag() { QuestionID = q.ID, TagID = t.ID, DateAdded = DateTime.Now });
                }
            }
            context.SaveChanges();
        }

        private List<Tag> AddTags(Entities context)
        {
            var tags = new List<Tag>()
            {
                new Tag() { Name = "Tag 1" , Description = "Tag 1"},
                new Tag() { Name = "Tag 2" , Description = "Tag 2"},
                new Tag() { Name = "Tag 3" , Description = "Tag 3"}
            };

            foreach (var tag in tags)
            {
                context.Tags.Add(tag);
            }

            context.SaveChanges();

            return tags;
        }


        private List<Question> AddQuestions(Entities context, List<int> usersIds)
        {
            var question1 = new Question()
            {
                Title = "First Question",
                Content = "Content of the first Question",
                DatePosted = DateTime.Now,
                Status = 1,
                UserID = usersIds.ElementAt(0),
                VoteCount = 9,
                NumberOfViews = 0
            };

            var question2 = new Question()
            {
                Title = "Second Question",
                Content = "Content of the second Question",
                DatePosted = DateTime.Now,
                Status = 1,
                UserID = usersIds.ElementAt(1),
                VoteCount = 9,
                NumberOfViews = 0
            };

            var questions = new List<Question>() { question1, question2 };

            foreach (var q in questions)
            {
                context.Questions.Add(q);
            }
            context.SaveChanges();

            return questions;
        }




    }
}
