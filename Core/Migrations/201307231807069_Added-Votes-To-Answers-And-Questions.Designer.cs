// <auto-generated />
namespace Core.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class AddedVotesToAnswersAndQuestions : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddedVotesToAnswersAndQuestions));
        
        string IMigrationMetadata.Id
        {
            get { return "201307231807069_Added-Votes-To-Answers-And-Questions"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
