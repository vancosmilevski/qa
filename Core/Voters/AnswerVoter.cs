﻿using QA.Model;
using System;

namespace Core
{
    public class AnswerVoter : PostVoter
    {
        private IUnitOfWork Uow;

        public AnswerVoter(IUnitOfWork uow)
        {
            Uow = uow;
        }

        public override int Vote(int answerID, VoteType voteType)
        {
            var answer = Uow.Answers.GetById(answerID);

            var answerVote = Uow.AnswerVotes.GetByIds(answerID, Uow.Users.CurrentUserId);

            bool answerVoteExists = answerVote != null;

            if (answerVoteExists)
            {
                UpdatePostVoteAndPostVoteCount(voteType, answer, answerVote);

                Uow.AnswerVotes.Update(answerVote);
            }
            else
            {
                Uow.AnswerVotes.Add(new AnswerVote()
                {
                    AnswerID = answerID,
                    UserID = Uow.Users.CurrentUserId,
                    Vote = (int)voteType,
                    DateVoted = DateTime.Now
                });

                //the first vote must be up or down
                if (voteType == VoteType.Up)
                    answer.VoteCount++;
                else
                    answer.VoteCount--;
            }

            Uow.Commit();

            return answer.VoteCount;
        }
    }
}
