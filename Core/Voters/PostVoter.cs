﻿using QA.Model;
using System;

namespace Core
{
    public abstract class PostVoter
    {
        public abstract int Vote(int postId, VoteType voteType);

        protected static void UpdatePostVoteAndPostVoteCount(VoteType voteType, IPost post, IPostVote postVote)
        {
            postVote.DateVoted = DateTime.Now;

            if (voteType == VoteType.Zero && (postVote.Vote == (int)VoteType.Up || postVote.Vote == (int)VoteType.Down))
            {
                if (postVote.Vote == (int)VoteType.Up)
                {
                    post.VoteCount--;
                }
                else
                {
                    post.VoteCount++;
                }
                postVote.Vote = (int)VoteType.Zero;
            }
            else if (voteType == VoteType.Up && (postVote.Vote == (int)VoteType.Down || postVote.Vote == (int)VoteType.Zero))
            {

                if (postVote.Vote == (int)VoteType.Down)
                {
                    post.VoteCount += 2;
                }
                else
                {
                    post.VoteCount += 1;
                }
                postVote.Vote = (int)VoteType.Up;
            }
            else if (voteType == VoteType.Down && (postVote.Vote == (int)VoteType.Up || postVote.Vote == (int)VoteType.Zero))
            {

                if (postVote.Vote == (int)VoteType.Up)
                {
                    post.VoteCount -= 2;
                }
                else
                {
                    post.VoteCount -= 1;
                }

                postVote.Vote = (int)VoteType.Down;
            }
            else
            {
                throw new Exception("Your conditions are bad and you should feel bad!");
            }
        }
    }
}
