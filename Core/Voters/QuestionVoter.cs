﻿using QA.Model;
using System;

namespace Core
{
    public class QuestionVoter : PostVoter
    {
        private IUnitOfWork Uow;

        public QuestionVoter(IUnitOfWork uow)
        {
            Uow = uow;
        }

        public override int Vote(int questionID, VoteType voteType)
        {
            var question = Uow.Questions.GetById(questionID);

            var questionVote = Uow.QuestionVotes.GetByIds(questionID, Uow.Users.CurrentUserId);

            bool questionVoteExists = questionVote != null;

            if (questionVoteExists)
            {
                UpdatePostVoteAndPostVoteCount(voteType, question, questionVote);

                Uow.QuestionVotes.Update(questionVote);
            }
            else
            {
                Uow.QuestionVotes.Add(new QuestionVote()
                {
                    QuestionID = questionID,
                    UserID = Uow.Users.CurrentUserId,
                    Vote = (int)voteType,
                    DateVoted = DateTime.Now
                });

                //the first vote must be up or down
                if (voteType == VoteType.Up)
                    question.VoteCount++;
                else
                    question.VoteCount--;
            }

            Uow.Commit();

            return question.VoteCount;
        }
    }
}
