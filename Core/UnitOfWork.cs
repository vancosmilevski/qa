﻿using QA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Core
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private Entities context = new Entities();

        private IQuestionRepository questionRepository;
        private IRepository<Answer> answerRepository;
        private IRepository<Tag> tagRepository;
        private IRepository<QuestionsTag> questionsTagsRepository;
        private IUserRepository userRepository;
        private IQuestionVoteRepository questionVotes;
        private IAnswerVoteRepository answerVotes;

        public void Commit()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed && disposing)
            {
                context.Dispose();
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public IQuestionRepository Questions
        {
            get
            {
                if (questionRepository == null)
                {
                    questionRepository = new QuestionRepository(context);
                }
                return questionRepository;
            }
        }

        public IRepository<Answer> Answers
        {
            get
            {
                if (answerRepository == null)
                {
                    answerRepository = new Repository<Answer>(context);
                }
                return answerRepository;
            }
        }

        public IRepository<QuestionsTag> QuestionsTags
        {
            get
            {
                if (questionsTagsRepository == null)
                {
                    questionsTagsRepository = new Repository<QuestionsTag>(context);
                }
                return questionsTagsRepository;
            }
        }

        public IUserRepository Users
        {
             get
            {
                if (userRepository == null)
                {
                    userRepository = new UserRepository(context);
                }
                return userRepository;
            }

        }

        public IRepository<Tag> Tags
        {
            get
            {
                if (tagRepository == null)
                {
                    tagRepository = new Repository<Tag>(context);
                }
                return tagRepository;
            }
        }


        public IQuestionVoteRepository QuestionVotes
        {
            get
            {
                if (questionVotes == null)
                {
                    questionVotes = new QuestionVoteRepository(context);
                }
                return questionVotes;
            }
        }

        public IAnswerVoteRepository AnswerVotes
        {
            get
            {
                if (answerVotes == null)
                {
                    answerVotes = new AnswerVoteRepository(context);
                }
                return answerVotes;
            }
        }
    }
}
