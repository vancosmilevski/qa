﻿using QA.Model;
using System.Linq;
using System.Web;

namespace Core
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(Entities context) : base(context) { }

        private int? currentUserId;

        public int CurrentUserId
        {
            get
            {
                if (!currentUserId.HasValue)
                {
                    currentUserId = this.GetAll().Single(u => u.Username == HttpContext.Current.User.Identity.Name).ID;
                }

                return currentUserId.Value;
            }
        }

        public string CurrentUserUsername
        {
            get
            {
                return HttpContext.Current.User.Identity.Name;
            }
        }
    }

}
