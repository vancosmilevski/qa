﻿using QA.Model;
using System;
using System.Linq;
using System.Web;

namespace Core
{
    public class QuestionRepository : Repository<Question>, IQuestionRepository
    {
        public QuestionRepository(Entities context) : base(context) { }

        public void AddQuestionTags(Question question, System.Collections.Generic.List<int> tagIDs)
        {
            DateTime dateAdded = DateTime.Now;
            foreach (var tagId in tagIDs)
            {
                question.QuestionsTags.Add(new QuestionsTag { QuestionID = question.ID, TagID = tagId, DateAdded = dateAdded });
            }
        }
    }
}
