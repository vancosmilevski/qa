﻿using QA.Model;
using System;
using System.Linq;

namespace Core
{
    public class QuestionVoteRepository : Repository<QuestionVote>, IQuestionVoteRepository
    {
        public QuestionVoteRepository(Entities context) : base(context) { }

        public QuestionVote GetByIds(int questionId, int userId)
        {
            return DbSet.FirstOrDefault(i => i.QuestionID == questionId && i.UserID == userId);
        }

        public override void Delete(int id)
        {
            throw new InvalidOperationException("Cannot delete a QuestionVote object by a single id value.");
        }

        public void Delete(int questionId, int userId)
        {
            var questionVote = GetByIds(questionId, userId);
            Delete(questionVote);
        }
    }
}
