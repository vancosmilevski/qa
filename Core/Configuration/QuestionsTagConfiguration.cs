﻿using QA.Model;
using System.Data.Entity.ModelConfiguration;

namespace Core
{
    public class QuestionsTagConfiguration : EntityTypeConfiguration<QuestionsTag>
    {
        public QuestionsTagConfiguration()
        {
            HasKey(qt => new { qt.QuestionID, qt.TagID });
        }
    }
}
