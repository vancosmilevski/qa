﻿using System.Collections.Generic;

namespace QA.Model
{
    public class Tag
    {
        public Tag()
        {
            QuestionsTags = new List<QuestionsTag>();
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual List<QuestionsTag> QuestionsTags { get; set; }
    }

}