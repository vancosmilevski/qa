﻿using System.Collections.Generic;

namespace QA.Model
{
    public class QuestionVote : QA.Model.IPostVote
    {
        public int ID { get; set; }
        public int Vote { get; set; }
        public int UserID { get; set; }
        public int QuestionID { get; set; }
        public System.DateTime DateVoted { get; set; }

        public virtual Question Question { get; set; }
        public virtual User User { get; set; }
    }
}