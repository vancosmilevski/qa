﻿
namespace QA.Model
{
    public class AnswerVote : QA.Model.IPostVote
    {
        public int ID { get; set; }
        public int Vote { get; set; }
        public int UserID { get; set; }
        public int AnswerID { get; set; }
        public System.DateTime DateVoted { get; set; }

        public virtual Answer Answer { get; set; }
        public virtual User User { get; set; }
    }
}