﻿using System;
namespace QA.Model
{
    public interface IPostVote
    {
        DateTime DateVoted { get; set; }
        int ID { get; set; }
        int UserID { get; set; }
        int Vote { get; set; }
    }
}
