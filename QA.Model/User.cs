﻿using System.Collections.Generic;

namespace QA.Model
{
    public class User
    {
        public User()
        {
            Answers = new List<Answer>();
            Questions = new List<Question>();
        }

        public int ID { get; set; }
        public string Username { get; set; }

        public virtual List<Answer> Answers { get; set; }
        public virtual List<Question> Questions { get; set; }
    }
}