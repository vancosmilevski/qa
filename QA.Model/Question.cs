﻿using System.Collections.Generic;

namespace QA.Model
{
    public class Question : QA.Model.IPost
    {
        public Question()
        {
            Answers = new List<Answer>();
            QuestionsTags = new List<QuestionsTag>();
        }

        public int ID { get; set; }
        public int VoteCount { get; set; }
        public int NumberOfViews { get; set; }
        public int Status { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public System.DateTime DatePosted { get; set; }
        public int UserID { get; set; }

        public virtual List<Answer> Answers { get; set; }
        public virtual User User { get; set; }
        public virtual List<QuestionsTag> QuestionsTags { get; set; }
        public virtual List<QuestionVote> Votes { get; set; }
    }
}