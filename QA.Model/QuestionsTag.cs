﻿
namespace QA.Model
{
    public class QuestionsTag
    {
        public int QuestionID { get; set; }
        public int TagID { get; set; }
        public System.DateTime DateAdded { get; set; }

        public virtual Question Question { get; set; }
        public virtual Tag Tag { get; set; }
    }
}