﻿using System;

namespace QA.Model
{
    public interface IPost
    {
        int ID { get; set; }
        int UserID { get; set; }
        int VoteCount { get; set; }
    }
}
