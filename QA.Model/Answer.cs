﻿using System.Collections.Generic;

namespace QA.Model
{
    public class Answer : QA.Model.IPost
    {
        public int ID { get; set; }
        public string Content { get; set; }
        public int VoteCount { get; set; }
        public int Status { get; set; }
        public System.DateTime DatePosted { get; set; }
        public int UserID { get; set; }
        public int QuestionID { get; set; }

        public virtual Question Question { get; set; }
        public virtual User User { get; set; }
        public virtual List<AnswerVote> Votes { get; set; }
    }
}