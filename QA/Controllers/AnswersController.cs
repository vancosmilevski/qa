﻿using Core;
using QA.Model;
using QA.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace QA.Controllers
{
    public class AnswersController : BaseController
    {
        private PostVoter voter;

        public AnswersController(IUnitOfWork uow, PostVoter voter)
        {
            this.voter = voter;
            Uow = uow;
        }

        [Authorize]
        [ValidateInput(false)]
        public ActionResult Create(int questionID, string content)
        {
            var answer = new Answer() { 
                Content = HtmlUtility.SanitizeHtml(content), 
                DatePosted = DateTime.Now,
                VoteCount = 0,
                UserID = Uow.Users.CurrentUserId,
                QuestionID = questionID
            };

            Uow.Answers.Add(answer);
            Uow.Commit();

            var answerViewModel = new AnswerViewModelForQuestionDetailsPage()
            {
                Content = HtmlUtility.SanitizeHtml(content),
                DatePosted = DateTime.Now,
                VoteCount = 0,
                IsAnswerFromCurrentUser = true,
                User = Uow.Users.GetById(Uow.Users.CurrentUserId)
            };

            return PartialView("partial_Answer", answerViewModel);
        }

        public ActionResult Edit(int id)
        {
            var answer = Uow.Answers.GetAll().SingleOrDefault(i => i.UserID == Uow.Users.CurrentUserId && i.ID == id);

            if (answer == null)
            {
                return View("PermissionsError");
            }

            AnswerEditViewModel viewModel = new AnswerEditViewModel()
            {
                ID = id,
                Content = answer.Content
            };

            return View(viewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(int id, string content)
        {
            var answer = Uow.Answers.GetAll().SingleOrDefault(i => i.UserID == Uow.Users.CurrentUserId && i.ID == id);

            if (answer == null)
            {
                return View("PermissionsError");
            }

            answer.Content = HtmlUtility.SanitizeHtml(content);
            
            Uow.Commit();

          //  return RedirectToAction("Details", "Questions", new { id = answer.QuestionID });
            return Redirect(Url.Action("Details", "Questions", new { id = answer.QuestionID }) + "#" + answer.ID);
        }

        public int PlaceVote(int answerID, int voteType)
        {
            int voteCount = voter.Vote(answerID, (VoteType)voteType);
            return voteCount;
        }

    }
}
