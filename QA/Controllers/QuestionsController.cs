﻿using Core;
using QA.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PagedList;
using QA.Model;

namespace QA.Controllers
{
    [Authorize]
    public class QuestionsController : BaseController
    {
        private PostVoter voter;

        public QuestionsController(IUnitOfWork uow, PostVoter voter)
        {
            this.voter = voter;
            Uow = uow;
        }

        [AllowAnonymous]
        public ActionResult Index(string q, int page = 1, int pagesize = 5, string sort = "newest")
        {
            var questions = Uow.Questions.GetAll()
                .Where(i => q == null || i.Title.Contains(q) || i.Content.Contains(q));

            switch (sort.ToEnum<QuestionsSortOrder>(defaultValue: QuestionsSortOrder.Newest))
            {
                case QuestionsSortOrder.Newest:
                    questions = questions.OrderByDescending(i => i.DatePosted);
                    break;
                case QuestionsSortOrder.Votes:
                    questions = questions.OrderByDescending(i => i.VoteCount);
                    break;
                case QuestionsSortOrder.Views:
                    questions = questions.OrderByDescending(i => i.NumberOfViews);
                    break;
            }

            IPagedList<Question> questionsPage = questions.ToPagedList(page, pagesize);

            return View(questionsPage);
        }

        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult Create(QuestionCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                Question question = new Question()
                {
                    Content = HtmlUtility.SanitizeHtml(model.Content),
                    Title = model.Title,
                    DatePosted = DateTime.Now,
                    VoteCount = 0,
                    NumberOfViews = 0,
                    Status = 1,
                    UserID = Uow.Users.CurrentUserId
                };

                Uow.Questions.Add(question);

                List<string> tags = model.Tags.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();

                List<int> tagIDs = Uow.Tags.GetAll().Where(t => tags.Contains(t.Name)).Select(t => t.ID).ToList();

                Uow.Questions.AddQuestionTags(question, tagIDs);

                Uow.Commit();

                return RedirectToAction("Index");
            }
            else
            {
                return View("Create");
            }
        }

        public ActionResult Edit(int id)
        {
            var question = Uow.Questions.GetAll().SingleOrDefault(i => i.UserID == Uow.Users.CurrentUserId && i.ID == id);

            if (question == null)
            {
                return View("PermissionsError");
            }

            var viewModel = new QuestionEditViewModel()
            {
                ID = id,
                Title = question.Title,
                Content = question.Content,
                Tags = String.Join(",", question.QuestionsTags.Select(i => i.Tag.Name))
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(QuestionEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var question = Uow.Questions.GetAll().SingleOrDefault(i => i.UserID == Uow.Users.CurrentUserId && i.ID == model.ID);

                if (question == null)
                {
                    return RedirectToAction("PermissionsError");
                }

                question.Title = model.Title;
                question.Content = model.Content;

                var questionTags = question.QuestionsTags.ToList();

                foreach (var questionTag in questionTags)
                {
                    Uow.QuestionsTags.Delete(questionTag);
                }

                List<string> tags = model.Tags.Split(",".ToArray(), StringSplitOptions.RemoveEmptyEntries).ToList();

                List<int> tagIDs = Uow.Tags.GetAll().Where(t => tags.Contains(t.Name)).Select(t => t.ID).ToList();

                Uow.Questions.AddQuestionTags(question, tagIDs);

                Uow.Commit();

                return RedirectToAction("Index");
            }
            else
            {
                return View("Edit", new { id = model.ID });
            }
        }

        [AllowAnonymous]
        public ActionResult Details(int id, int page = 1, string answerSort = "votes")
        {
            var question = Uow.Questions.GetById(id);
            question.NumberOfViews++;
            Uow.Commit();

            // this is a big mess, fix it later on
            IEnumerable<Answer> answers = Uow.Answers.GetAll().Where(i => i.QuestionID == id);

            if (answerSort == "oldest")
            {
                answers = answers.OrderByDescending(i => i.DatePosted).ToList();
            }
            else
            {
                answers = answers.OrderByDescending(i => i.VoteCount).ToList();
            }

            var questionVoteType = question.Votes.SingleOrDefault(i => i.UserID == Uow.Users.CurrentUserId);

            List<AnswerViewModelForQuestionDetailsPage> partialAnswers = new List<AnswerViewModelForQuestionDetailsPage>();

            foreach (var answer in answers)
            {
                var answerVoteType = answer.Votes.SingleOrDefault(i => i.UserID == Uow.Users.CurrentUserId);

                partialAnswers.Add(new AnswerViewModelForQuestionDetailsPage()
                {
                    ID = answer.ID,
                    Content = answer.Content,
                    VoteCount = answer.VoteCount,
                    DatePosted = answer.DatePosted,
                    User = answer.User,
                    AnswerVoteType = (answerVoteType != null) ? answerVoteType.Vote : 0,
                    IsAnswerFromCurrentUser = answer.UserID == Uow.Users.CurrentUserId
                });

            }

            QuestionDetailsViewModel viewModel = new QuestionDetailsViewModel()
            {
                ID = question.ID,
                Question = question,
                QuestionVoteType = questionVoteType != null ? questionVoteType.Vote : 0,
                Answers = partialAnswers.ToPagedList(page, 5)
            };

            return View(viewModel);
        }



        public int PlaceVote(int questionID, int voteType)
        {
            int voteCount = voter.Vote(questionID, (VoteType)voteType);
            return voteCount;
        }
    }
}
