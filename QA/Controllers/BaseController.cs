﻿using Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace QA.Controllers
{
    public class BaseController : Controller
    {
        protected IUnitOfWork Uow { get; set; }
    }
}
