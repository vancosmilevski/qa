﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QA
{
    public enum QuestionStatus
    {
        Unanswered,
        Answered,
        AnsweredAccepted,
        Flaged
    }

    public enum AnswerStatus
    {
        Normal,
        Accepted,
        Flaged
    }

    public enum QuestionsSortOrder
    {
        Newest,
        Votes,
        Views
    }
}