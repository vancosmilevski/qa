﻿using Ninject.Modules;
using Core;
using QA.Controllers;

namespace QA
{
    public class NinjectBindings : NinjectModule
    {
        public override void Load()
        {
            Bind(typeof(IRepository<>)).To(typeof(Repository<>));
            //Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();
            Bind<IUnitOfWork>().To<UnitOfWork>();

            Bind<PostVoter>().To<QuestionVoter>().WhenInjectedInto(typeof(QuestionsController));
            Bind<PostVoter>().To<AnswerVoter>().WhenInjectedInto(typeof(AnswersController));
        }
    }
}