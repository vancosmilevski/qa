﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace QA
{
    public static class Globals
    {
        public static bool UseSignalR 
        {
            get 
            {
                return ConfigurationManager.AppSettings["UseSignalR"] == "true";        
            }
        }
    }

    public static class StringHelpers
    {
        public static string ToSeoUrl(this string url)
        {
            // make the url lowercase
            string encodedUrl = (url ?? "").ToLower();

            // replace & with and
            encodedUrl = Regex.Replace(encodedUrl, @"\&+", "and");

            // remove characters
            encodedUrl = encodedUrl.Replace("'", "");

            // remove invalid characters
            encodedUrl = Regex.Replace(encodedUrl, @"[^a-z0-9]", "-");

            // remove duplicates
            encodedUrl = Regex.Replace(encodedUrl, @"-+", "-");

            // trim leading & trailing characters
            encodedUrl = encodedUrl.Trim('-');

            return encodedUrl;
        }

        public static T ToEnum<T>(this string sortString, T defaultValue)
        {
            foreach (var item in Enum.GetNames(typeof(T)))
            {
                if (sortString.ToLower() == item.ToLower())
                {
                    return (T)Enum.Parse(typeof(T), sortString, ignoreCase: true);
                }
            }

            return defaultValue;
        }
    }

    public static class Helpers
    {
        public static string ToNicelyFormattedDateTime(this DateTime dateTimePosted)
        {
            DateTime now = DateTime.Now;
            TimeSpan timeSpan = now.Subtract(dateTimePosted);

            if (timeSpan.TotalSeconds < 59)
            {
                return (Convert.ToInt32(timeSpan.TotalSeconds)).ToString() + " sec ago";
            }

            if (timeSpan.TotalMinutes < 59)
            {
                string min = (Convert.ToInt32(timeSpan.TotalMinutes)).ToString();
                return min != "1" ? (min + " mins ago") : "1 min ago";
            }
            if (timeSpan.TotalHours < 23)
            {
                string h = (Convert.ToInt32(timeSpan.TotalHours)).ToString();

                return h != "1" ? (h + " hours ago") : "1 hour ago";
            }
            if (timeSpan.TotalDays < 4)
            {
                string days = (Convert.ToInt32(timeSpan.TotalDays)).ToString();
                return days == "1" ? "1 day ago" : (days + " days ago");
            }
            if (now.Year != dateTimePosted.Year)
            {
                return (dateTimePosted.ToString("MMM dd", CultureInfo.CreateSpecificCulture("en-Us")) +
                " '" + dateTimePosted.ToString("yy") + " at " + dateTimePosted.ToShortTimeString());
            }

            return (dateTimePosted.ToString("MMM dd", CultureInfo.CreateSpecificCulture("en-Us")) +
                " at " + dateTimePosted.ToShortTimeString());
        }


        public static string Truncate(this HtmlHelper helper, string input, int length)
        {
            if (input.Length <= length)
                return input;
            else
                return input.Substring(0, length) + "...";
        }

        public static string GetClassForQuestionStatus(this HtmlHelper helper, int status, int totalAnswers)
        {
            return GetClassForQuestionStatus(helper, (QuestionStatus)status, totalAnswers);
        }

        public static string GetClassForQuestionStatus(this HtmlHelper helper, QuestionStatus status, int totalAnswers)
        {
           // return "answered-accepted";
            if (totalAnswers == 0 || status == QuestionStatus.Unanswered)
                return "unanswered";
            else if (status == QuestionStatus.Answered)
                return "answered";
            else if (status == QuestionStatus.AnsweredAccepted)
                return "answered-accepted";
            return "answered";
        }

        public static string RenderPartialToString(string view, object model, ControllerContext Context)
        {
            if (string.IsNullOrEmpty(view))
            {
                view = Context.RouteData.GetRequiredString("action");
            }

            ViewDataDictionary ViewData = new ViewDataDictionary();

            TempDataDictionary TempData = new TempDataDictionary();

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(Context, view);

                ViewContext viewContext = new ViewContext(Context, viewResult.View, ViewData, TempData, sw);

                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        public static string RenderPartialView(string controllerName, string partialView, object model)
        {
            var context = new HttpContextWrapper(System.Web.HttpContext.Current) as HttpContextBase;

            var routes = new System.Web.Routing.RouteData();
            routes.Values.Add("controller", controllerName);

            var requestContext = new RequestContext(context, routes);

            string requiredString = requestContext.RouteData.GetRequiredString("controller");
            var controllerFactory = ControllerBuilder.Current.GetControllerFactory();
            var controller = controllerFactory.CreateController(requestContext, requiredString) as ControllerBase;

            controller.ControllerContext = new ControllerContext(context, routes, controller);

            var ViewData = new ViewDataDictionary();

            var TempData = new TempDataDictionary();

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, partialView);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, ViewData, TempData, sw);

                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
    }
    
   
}