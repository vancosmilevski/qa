﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace QA
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "FriendlyUrl",
            //    url: "question/{id}/{slug}",
            //    defaults: new { controller = "Question", action = "Details", slug = UrlParameter.Optional },
            //    constraints: new { id = "^[0-9]+$" }
            //);

            routes.MapRoute(
                name: "FriendlyUrl",
                url: "questions/details/{id}/{slug}",
                defaults: new { controller = "Questions", action = "Details", slug = UrlParameter.Optional },
                constraints: new { id = "^[0-9]+$" }
            );


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}/{slug}",
                defaults: new { controller = "Questions", action = "Index", id = UrlParameter.Optional, slug = UrlParameter.Optional }
            );
        }
    }
}