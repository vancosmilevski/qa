﻿var QA = {};
(function (QA) {

    QA.voteType = {
        up: 1,
        down: 2,
        zero: 3
    };

    var ajaxFormSubmit = function () {
        var $form = $(this);
        var options = {
            url: $form.attr("action"),
            type: $form.attr("method"),
            data: $form.serialize()
        };

        $.ajax(options).done(function (data) {
            var $target = $($form.attr('data-target'));
            $target.replaceWith(data);
        });

        return false;
    }

    // $("form[data-ajax-enabled='true']").submit(ajaxFormSubmit);


    var getPage = function () {
        var $a = $(this);

        var options = {
            url: $a.attr('href'),
            data: $('form').serialize(),
            type: "get"
        };

        $.ajax(options).done(function (data) {
            var target = $a.parents("div.pager").attr("data-target");
            $(target).replaceWith(data);
        });

        return false;
    };

    //$(".main-content").on("click",".pager a", getPage);
}(QA));