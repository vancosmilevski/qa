﻿var voter = (function () {

    $(document).ajaxError(function (event, xhr) {
        alert(xhr.status + ":" + xhr.statusText);
    });

    //var questionVoteUp = function (questionID) {
    //    return questionVote(questionID, true);
    //};

    //var questionVoteDown = function (questionID) {
    //    return questionVote(questionID, false);
    //};

    var questionVote = function (questionID, voteType) {
        return $.post('/Questions/PlaceVote', { questionID: questionID, voteType: voteType });
    };

    answerVote = function (answerID, voteType) {
        return $.post('/Answers/PlaceVote', { answerID: answerID, voteType: voteType });
    };

    return {
        //questionVoteUp: questionVoteUp,
        //questionVoteDown: questionVoteDown,
        questionVote: questionVote,
        answerVote: answerVote
    }
}());