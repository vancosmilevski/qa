﻿$(function () {

    var answerTemplate = Handlebars.compile($("#answerTemplate").html());

    var questionId = $("#question-id").val();

    var votesUpdater = $.connection.votesUpdaterHub;

    votesUpdater.client.updateVoteCount = function(voteCount, answerId, isQuestion)
    {
        console.log("updateVoteCount",voteCount, answerId, isQuestion);
        //debugger;
        if (isQuestion) {
            $('#vote-count-holder').html(voteCount);   
        }
        else
        {
            $('#vote-count-holder-answer-'+ answerId).html(voteCount);       
        }
    };

    votesUpdater.client.updateVoteCount2 = function(voteCount, answerId, isQuestion)
    {
        console.log("updateVoteCount2",voteCount, answerId, isQuestion);
    };

    votesUpdater.client.answerResponse = function(answerId, voteCount, content, dateAnswered, username, questionCount, answerCount)
    {
        //debugger;
        console.log("answerResponse ", answerId, voteCount, content, dateAnswered, username, questionCount, answerCount);
        currentUserUsername = $('#current-user-username').val();
        var output = answerTemplate(
            { 
                answerId: answerId, 
                voteCount: voteCount, content: content, 
                dateAnswered: dateAnswered, 
                username: username, 
                questionCount: questionCount, 
                answerCount: answerCount,
                showEditLink: currentUserUsername === username,
                edithref: '/Answers/Edit/' + answerId
            });

            $('#answers .answer:last').after(output);
    };


    $.connection.hub.start().done(function(){
                
        votesUpdater.server.addGroup(questionId);


        var $answerForm = $('#user-answer-form');
        $answerForm.submit(function () {

            tinymce.get('user-answer-content').save();
            debugger;
            var content = $('#user-answer-content').val();

            votesUpdater.server.answerCreate(questionId, content);
            tinymce.get("user-answer-content").setContent('');
            //$.post("/Answers/Create", $answerForm.serialize(), function (data) {
            //    //console.log(data);
            //    $('#answers').append(data);
            //    //$answerForm.find('textarea').val('');
            //    tinymce.get("user-answer-content").setContent('')
            //});

            return false;
        });

        $('#vote-up').click(function () {
            votesUpdater.server.placeVote('true', questionId, 'true', null);
        });

        $('#vote-down').click(function () {
            votesUpdater.server.placeVote('false', questionId, 'true', null);
        });

        $(document).on('click', '.vote-up-answer', function () {
            var  answerId = $(this).data('answer-id');
            votesUpdater.server.placeVote('true', questionId, 'false', answerId);
        });

        $(document).on('click','.vote-down-answer', function () {
            var answerId = $(this).data('answer-id');
            votesUpdater.server.placeVote('false', questionId, 'false', answerId);
        });
    });

});