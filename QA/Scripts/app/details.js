﻿(function () {

    var $answerForm = $('#user-answer-form');

    $answerForm.submit(function () {

        tinymce.get('user-answer-content').save();

        $.post("/Answers/Create", $answerForm.serialize(), function (data) {
            //console.log(data);
            //$('#answers').append(data);
            $('#answers .answer:last').after(output);
            //$answerForm.find('textarea').val('');
            tinymce.get("user-answer-content").setContent('');
        });

        return false;
    });

}());

(function () {
    
    var questionId = $("#question-id").val();

    var $voteUp = $('#vote-up');
    var $voteDown = $('#vote-down');

    var getVoteTypeAndUpdateActiveButtonFor_UP_Vote = function ($voteUp, $voteDown) {
        var voteType;

        if ($voteUp.hasClass('vote-up-on')) {
            $voteUp.removeClass('vote-up-on');
            voteType = QA.voteType.zero;
        }
        else {
            $voteUp.addClass('vote-up-on');
            $voteDown.removeClass('vote-down-on');
            voteType = QA.voteType.up;
        }

        return voteType;
    };

    var getVoteTypeAndUpdateActiveButtonFor_DOWN_Vote = function ($voteUp, $voteDown) {
        var voteType;

        if ($voteDown.hasClass('vote-down-on')) {
            $voteDown.removeClass('vote-down-on');
            voteType = QA.voteType.zero;
        }
        else {
            $voteDown.addClass('vote-down-on');
            $voteUp.removeClass('vote-up-on');
            voteType = QA.voteType.down;
        }

        return voteType;
    };



    var updateQuestionVotesUp = function () {
        var voteType = getVoteTypeAndUpdateActiveButtonFor_UP_Vote($voteUp, $voteDown);
        voter.questionVote(questionId, voteType).done(updateQuestionVoteCount);
    };

    var updateQuestionVotesDown = function () {
        var voteType = getVoteTypeAndUpdateActiveButtonFor_DOWN_Vote($voteUp, $voteDown);
        voter.questionVote(questionId, voteType).done(updateQuestionVoteCount);
    };

    var updateQuestionVoteCount = function (data) {
        $('#vote-count-holder').html(data);
    };

    var updateAnswerVotesUp = function () {

        var $voteUp = $(this);
        var $voteDown = $voteUp.siblings('.vote-down-answer');
        var answerId = $voteUp.data('answer-id');
        var voteType = getVoteTypeAndUpdateActiveButtonFor_UP_Vote($voteUp, $voteDown);

        voter.answerVote(answerId, voteType).done(function (data) {
            updateAnswerVoteCount(data, answerId);
        });
    };

    var updateAnswerVotesDown = function () {

        var $voteDown = $(this);
        var $voteUp = $voteDown.siblings('.vote-up-answer');
        var answerId = $voteDown.data('answer-id');
        var voteType = getVoteTypeAndUpdateActiveButtonFor_DOWN_Vote($voteUp, $voteDown);

        voter.answerVote(answerId, voteType).done(function (data) {
            updateAnswerVoteCount(data, answerId);
        });
    };


    var updateAnswerVoteCount = function (data, answerId) {
        $('#vote-count-holder-answer-' + answerId).html(data);
    };

    var wireEvents = function () {

        $voteUp.click(updateQuestionVotesUp);
        $voteDown.click(updateQuestionVotesDown);

        $('#answers').on('click', '.vote .vote-up-answer', updateAnswerVotesUp);
        $('#answers').on('click', '.vote .vote-down-answer', updateAnswerVotesDown);

    };

    $(function () {
        wireEvents();
    });

}());