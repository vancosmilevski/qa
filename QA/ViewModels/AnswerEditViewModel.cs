﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using PagedList;

namespace QA
{
    public class AnswerEditViewModel
    {
        public int ID { get; set; }

        [Required]
        [StringLength(4000, MinimumLength = 5)]
        [AllowHtml]
        public string Content { get; set; }
    }
}