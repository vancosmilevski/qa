﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace QA
{
    public class QuestionCreateViewModel
    {
        [Required]
        [StringLength(150, MinimumLength=2)]
        public string Title { get; set; }

        [Required]
        [StringLength(4000, MinimumLength=5)]
        [AllowHtml]
        public string Content { get; set; }

        [Required(ErrorMessage="Please enter at lesat 1 tag")]
        public string Tags { get; set; }
    }
}