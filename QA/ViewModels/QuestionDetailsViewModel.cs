﻿using PagedList;
using QA.Model;

namespace QA
{
    public class QuestionDetailsViewModel
    {
        public int ID { get; set; }
        public Question Question { get; set; }
        public IPagedList<AnswerViewModelForQuestionDetailsPage> Answers { get; set; }

        public int QuestionVoteType { get; set; }
    }

    public class AnswerViewModelForQuestionDetailsPage
    {
        public int ID { get; set; }
        public string Content { get; set; }
        public int VoteCount { get; set; }
        public System.DateTime DatePosted { get; set; }
        public virtual User User { get; set; }

        public int AnswerVoteType { get; set; }

        public bool IsAnswerFromCurrentUser { get; set; }
    }
}