﻿using Core;
using Microsoft.AspNet.SignalR;
using QA.Model;
using QA.Models;
using System;
using System.Threading.Tasks;

namespace QA
{
    public class VotesUpdaterHub : Hub
    {
        private UnitOfWork Uow;

        public VotesUpdaterHub(UnitOfWork unitOfWork)
        {
            // TODO: Complete member initialization
            this.Uow = unitOfWork;
        }
        public Task AddGroup(string questionID)
        {
            return Groups.Add(Context.ConnectionId, questionID);
        }

        public void PlaceVote(bool isUpVote, int questionID, bool isQuestion, int? answerId = null)
        {
            //var ctx = new Entities();
            if (isQuestion)
            {
                var question = Uow.Questions.GetById(questionID);

                if (isUpVote)
                    question.VoteCount++;
                else
                    question.VoteCount--;

                Uow.Commit();
                //ctx.SaveChanges();
                //return question.VoteCount;
                //Clients.All.updateVoteCount(question.VoteCount);

                Clients.Group(questionID.ToString()).updateVoteCount(question.VoteCount, questionID, isQuestion);
            }
            else
            {
                var answer = Uow.Answers.GetById(answerId.Value);

                if (isUpVote)
                    answer.VoteCount++;
                else
                    answer.VoteCount--;

                Uow.Commit();
                //ctx.SaveChanges();
                Clients.Group(questionID.ToString()).updateVoteCount(answer.VoteCount, answerId.Value, isQuestion);
                Clients.Group(questionID.ToString()).updateVoteCount2(answer.VoteCount, answerId.Value, isQuestion);
            }
        }

        public void AnswerCreate(int questionID, string content)
        {
            //var ctx = new Entities();
            var question = Uow.Questions.GetById(questionID);

            //question.NumberOfViews = 21;
            var answer = new Answer()
            {
                Content = HtmlUtility.SanitizeHtml(content),
                DatePosted = DateTime.Now,
                VoteCount = 0,
                UserID = Uow.Users.CurrentUserId,
                QuestionID = questionID
            };

            //answer.User = ctx.Users.SingleOrDefault(u => u.Username == HttpContext.Current.User.Identity.Name);

            //question.Answers.Add(answer);

            Uow.Answers.Add(answer);

            Uow.Commit();

           // ctx.SaveChanges();

            Clients.Group(questionID.ToString()).answerResponse(answer.ID, answer.VoteCount, answer.Content, answer.DatePosted.ToNicelyFormattedDateTime(), answer.User.Username, answer.User.Questions.Count, answer.User.Answers.Count);

        }


    }
}